#include <SFML/Graphics.hpp>
#include "Utils.h"
#include "Game.h"


int main()
{
  sf::RenderWindow window(sf::VideoMode(Utils::window_x, Utils::window_y), "Squash Game", sf::Style::Titlebar|sf::Style::Close);
  
  while (window.isOpen())
  {
    Utils::printMessageAt(window, "Press space to start", Utils::Point(Utils::window_x / 7, Utils::window_y / 2.5), 50);
    sf::sleep(sf::seconds(0.1));

    while (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space) and window.isOpen())
    {
      sf::Event event;
    
      while (window.pollEvent(event))
        if (event.type == sf::Event::Closed or sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
          window.close();
    }

    if(window.isOpen())
    {
      Game game(window); 
      game.run();
    }
  }
}
