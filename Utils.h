#ifndef UTILS_H
#define UTILS_H

#include <SFML/Graphics.hpp>
#include <math.h>


namespace Utils
{
  const double Pi = 3.141592653589793238463;

  const unsigned window_x = 600;
  const unsigned window_y = 400;

  const std::string font_address = "media/Ubuntu-R.ttf";

  const std::string wall_sound_address = "media/wood.ogg";
  const std::string racquet_sound_address = "media/racquet.ogg";
  const std::string racquet2_sound_address = "media/racquet_moving.ogg";
  const std::string applause_sound_address = "media/applause.ogg";
  const std::string loss_sound_address = "media/loss.ogg";
  const std::string music_address = "media/music.ogg";

  const int line_w = round(0.0066 * window_x);
  const int left_wall_d = line_w;
  const int right_wall_d = window_x - line_w;
  const int top_wall_d = round(window_y / 13);

  const int racquet_x = round(0.117 * window_x);
  const int racquet_y = round(0.0175 * window_y);
  const int catchline_y = round(0.875 * window_y);

  const int ball_r = round(0.025 * window_y);
  const int ball_start_height = round(0.2 * window_y);
  const double ball_speed = round(0.03 * window_y);
  const double ball_angle = 1.6 * Pi;

  const int nozone_top = top_wall_d + line_w + ball_r;
  const int nozone_left = left_wall_d + line_w + ball_r;
  const int nozone_right = right_wall_d - line_w - ball_r;
  const int nozone_catchline = catchline_y - racquet_y / 2 - ball_r;

  struct Point
  {
    int x;
    int y;

    Point() {}
    Point(int xx, int yy) : x(xx), y(yy) {}
    Point(const Point& d) { x = d.x; y = d.y; }
    ~Point() {}
  };

  struct PrecisePoint
  {
    double x;
    double y;

    PrecisePoint() {}
    PrecisePoint(double xx, double yy) : x(xx), y(yy) {}
    PrecisePoint(const PrecisePoint& d) { x = d.x; y = d.y; }
    ~PrecisePoint() {}
  };

  struct Borders
  {
    Point start;
    Point end;

    Borders() {}
    Borders(Point s, Point e) : start(s), end(e) {} 
    Borders(int x1, int y1, int x2, int y2) : start(x1, y1), end(x2, y2) {}
    Borders(const Borders& d) { start = d.start; end = d.end; }
    ~Borders() {}
  };

  void printMessage(sf::RenderWindow& window, const std::string& message, bool clean = true);
  void printMessageAt(sf::RenderWindow& window, const std::string& message, Point point, int text_size, bool clean = true);
}

#endif
