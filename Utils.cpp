#include <SFML/Graphics.hpp>
#include "Utils.h"


void Utils::printMessageAt(sf::RenderWindow& window, const std::string& message, Point point, int text_size, bool clean)
{
  if (clean)
    window.clear();

  sf::Font font;
  if (font.loadFromFile(font_address))
  {
    sf::Text text;
    text.setFont(font);
    text.setString(message);

    text.setCharacterSize(text_size);
    text.setColor(sf::Color::White);
    text.setPosition(point.x, point.y);

    window.draw(text);
    window.display();
  }
}

void Utils::printMessage(sf::RenderWindow& window, const std::string& message, bool clean)
{
  if (clean)
    window.clear();

  sf::Font font;
  if (font.loadFromFile(font_address))
  {
    sf::Text text;
    int size = int(Utils::window_x / message.size() / 0.6286);
    text.setFont(font);
    text.setString(message);

    text.setCharacterSize(size);
    text.setColor(sf::Color::White);
    text.setPosition(0, (window_y - size) / 2);

    window.draw(text);
    window.display();
  }
}
