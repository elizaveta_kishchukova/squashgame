#ifndef WALL_H
#define WALL_H

#include "Thing.h"
#include "Utils.h"


class Wall : public Thing
{
  int length;
  int height;

public:
  Wall(int x, int y, int len, int hei) : Thing(), length(len), height(hei) { setPosition(x, y); }
  ~Wall() override {};

  void changePosition(int xx, int yy) override { setPosition(xx, yy); }
  void setDimentions(int l, int h) { length = l; height = h; }
  sf::Vector2f getDimentions() const { return sf::Vector2f(length, height); }

  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  Utils::Borders getBorders() const override;
};

#endif
