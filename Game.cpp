#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include "Game.h"
#include "Utils.h"
#include "Ball.h"
#include "Racquet.h"
#include <time.h>
#include <fstream>


Game::Game(sf::RenderWindow& win) : ball(int(Utils::window_x / 2 - Utils::ball_r), Utils::ball_start_height, Utils::ball_r, Utils::ball_angle, Utils::ball_speed),
               racquet(int(Utils::window_x / 2 - Utils::racquet_x / 2), Utils::catchline_y, Utils::racquet_x, Utils::racquet_y),
               window(win)
{  
  using namespace Utils;
  
  walls.push_back(std::unique_ptr<Wall> (new Wall(int(window_x / 2), top_wall_d + int(line_w / 2), window_x, line_w)));
  walls.push_back(std::unique_ptr<Wall> (new Wall(left_wall_d + int(line_w / 2), int(window_y / 2), line_w, window_x)));
  walls.push_back(std::unique_ptr<Wall> (new Wall(right_wall_d - int(line_w / 2), int(window_y / 2), line_w, window_x)));
  
  sf::Mouse::setPosition(sf::Vector2i(int(window_x / 2), catchline_y), window);

  start_time = std::time(NULL);
  score = 0;

  music_init(wall_sound, racquet_sound, racquet2_sound, applause_sound, loss_sound, music);
  music.play();
}

void Game::music_init(sf::Music& wall_s, sf::Music& racquet_s, sf::Music& racquet2_s,
                sf::Music& applause_s, sf::Music& loss_s, sf::Music& music_s)
{
  using namespace Utils;

  wall_s.openFromFile(wall_sound_address);
  racquet_s.openFromFile(racquet_sound_address);
  racquet2_s.openFromFile(racquet2_sound_address);
  applause_s.openFromFile(applause_sound_address);
  loss_s.openFromFile(loss_sound_address);
  music_s.openFromFile(music_address);

  racquet_s.setVolume(25);
  racquet2_s.setVolume(20);
  applause_s.setVolume(50);
  loss_s.setVolume(50);

  music_s.setLoop(true);
}

void Game::run()
{

  while (!isOver())
  {
    sf::Event event;
    
    while (window.pollEvent(event))
      if (event.type == sf::Event::Closed or sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        window.close();

    update(window);
    show(window);
  }
  
  music.stop();
  Utils::printMessage(window, "GAME OVER");
  sf::sleep(sf::seconds(0.5));
  putScoreUp();
  waitForSpace();
}

void Game::waitForSpace() const
{
  Utils::printMessageAt(window, "Press space to continue", Utils::Point(0, Utils::window_y / 1.1), 30, false);
 
  while(true)
  {
    sf::Event event;
    
    while (window.pollEvent(event))
      if (event.type == sf::Event::Closed or sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        window.close();

    if (!window.isOpen() or sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
      break;
  }
}

void Game::update(sf::RenderWindow& window)
{
  for (unsigned i = 0; i < walls.size(); ++i)
    if (areColliding(&ball, walls[i].get()))
      handleBallCollision(walls[i]->getBorders(), walls[i]->getSpeed(), walls[i]->getAngle());

  if (areColliding(&ball, &racquet))
  {
    handleBallCollision(racquet.getBorders(), racquet.getSpeed(), racquet.getAngle());
    ++score;
  }
 
  ball.updatePosition();
  racquet.updatePosition(sf::Mouse::getPosition(window).x); 
}

void Game::show(sf::RenderWindow& window) const
{
  window.clear();
  
  window.draw(ball);
  window.draw(racquet);

  for (unsigned i = 0; i < walls.size(); ++i)
    window.draw(*walls[i].get());

  Utils::printMessageAt(window, "Seconds: " + std::to_string(std::time(NULL) - start_time) + "    Score: " + std::to_string(score),
                        Utils::Point(Utils::line_w * 2, 0), int(Utils::window_y / 14), false);
  window.display();
} 

bool Game::areColliding(Thing* p_thing1, Thing* p_thing2) const 
{
  Utils::Borders borders1 = p_thing1->getBorders();
  Utils::Borders borders2 = p_thing2->getBorders();

  bool colliding [] {false, false};

  if ((borders1.start.x <= borders2.start.x and borders1.end.x >= borders2.start.x) or
      (borders1.start.x <= borders2.end.x   and borders1.end.x >= borders2.end.x)   or
      (borders1.start.x >= borders2.start.x and borders1.end.x <= borders2.end.x)   )
    colliding[0] = true;

  if ((borders1.start.y <= borders2.start.y and borders1.end.y >= borders2.start.y) or
      (borders1.start.y <= borders2.end.y   and borders1.end.y >= borders2.end.y)   or
      (borders1.start.y >= borders2.start.y and borders1.end.y <= borders2.end.y)   )
    colliding[1] = true;

  return colliding[0] && colliding[1];
}

double Game::balanceAngle(double a) const 
{
  a = a - 2 * Utils::Pi * int(a / (2 * Utils::Pi));

  if (a < 0)
    a += 2 * Utils::Pi;

  return a;
}

void Game::handleBallCollision(const Utils::Borders& borders, const int thing_speed, const double thing_angle)
{
  double angle = ball.getAngle();
  int speed = ball.getSpeed();
  
  if (ball.getPosition().y <= borders.start.y)
  {
    angle = 2 * Utils::Pi - angle;
    
    if (angle != thing_angle && thing_speed != 0)
    {
      double side = 0;
      
      if (thing_angle == 0)
      {
        side = sqrt(speed * speed + thing_speed * thing_speed - 2 * speed * thing_speed * cos(Utils::Pi - angle));
        angle = acos((side * side + thing_speed * thing_speed - speed * speed) / (2 * side * thing_speed));
      }
      else
      {
        side = sqrt(speed * speed + thing_speed * thing_speed - 2 * speed * thing_speed * cos(angle));
        angle = Utils::Pi - acos((side * side + thing_speed * thing_speed - speed * speed) / (2 * side * thing_speed));
      }

      if (angle == 0)
        angle = 0.05 * Utils::Pi;

      if (angle == Utils::Pi)
        angle = 0.95 * Utils::Pi;
   
      racquet2_sound.play();
    }

    else
      racquet_sound.play();
  }

  else 
  {
    if (ball.getPosition().y >= borders.end.y and angle <= Utils::Pi)
      angle = 2 * Utils::Pi - angle; 
    else
      angle = balanceAngle(1 * Utils::Pi - angle);

    wall_sound.play();
  }

   if (angle == 0)
     angle = 1.05 * Utils::Pi;

   if (angle == Utils::Pi)
     angle = 1.95 * Utils::Pi;

   ball.setAngle(angle);
}

bool Game::isOver() const
{
  if (ball.getPosition().y > Utils::window_y)
    return true;

  return false;
}

void Game::putScoreUp()
{
  long seconds = std::time(NULL) - start_time;
  std::vector<long> score_table;

  readScoreIn(score_table);
  unsigned place = putScoreInPlace(score_table);
  
  if (place)
    applause_sound.play();
  else
    loss_sound.play();

  std::string message = "You scored " + std::to_string(score) + " points in " + std::to_string(seconds) + " seconds. TOP 10 scores:";

  printMessageAt(window, message, Utils::Point(0, 0), Utils::window_x / 24);
  printScore(score_table, place);
  saveScore(score_table);
}

void Game::readScoreIn(std::vector<long>& score_table) const
{
  std::ifstream file;
  file.open("score.sc");

  if (file.is_open())
  {    
    for (int i = 0; i < 10; ++i)
    {
      long sc;
      if(file >> sc)
        score_table.push_back(sc);
      else 
        break;

      file.ignore(1);
    }
    file.close();
  }
}

unsigned Game::putScoreInPlace(std::vector<long>& score_table) const
{
  unsigned place = 0;

  for (unsigned i = 0; i < score_table.size(); ++i)
    if (score > score_table[i])
    {
      score_table.insert(score_table.begin() + i, score);
      place = i + 1;
      if (score_table.size() > 10)
        score_table.pop_back();

      break;
    }

  if (!place and score_table.size() < 10)
  {
    score_table.push_back(score);
    ++place;
  }

  return place;
}

void Game::printScore(std::vector<long>& score_table, unsigned place) const
{
  for (unsigned i = 0; i < score_table.size(); ++i)
  { 
    if (i + 1 == place)
    {
      sf::RectangleShape winner_highlight(sf::Vector2f(Utils::window_x, int(Utils::window_y / 16) + 5));
      winner_highlight.setPosition(0, 52 + 30 * i);
      winner_highlight.setFillColor(sf::Color(50, 50, 50));
      window.draw(winner_highlight);
    }
      
    Utils::printMessageAt(window, std::to_string(i + 1) + ": " + std::to_string(score_table[i]), 
                          Utils::Point(0, 50 + 30 * i), int(Utils::window_y / 16), false);
  }
}

void Game::saveScore(std::vector<long>& score_table) const
{
  std::ofstream file;
  file.open("score.sc");

  for (unsigned i = 0; i < score_table.size(); ++i)
    file << score_table[i] << ' ';

  file.close();
}
