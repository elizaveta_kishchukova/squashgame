#ifndef BALL_H
#define BALL_H

#include "Thing.h"
#include "Racquet.h" 
#include "Utils.h"


class Ball : public Thing
{
  Utils::PrecisePoint xy;
  int radius;

public:
  Ball() : Thing() {}
  Ball(int xx, int yy, int rad, double ang, double spe);
  ~Ball() override {};

  void updatePosition() override;
  
  void setRadius(int r) { radius = r; }
  int getRadius() const { return radius; }
  void changePosition(int xx, int yy) override;

  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  Utils::Borders getBorders() const override;
};

#endif
