#include <SFML/Graphics.hpp>
#include "Racquet.h"
#include "Thing.h"
#include "Utils.h"
#include <math.h>


void Racquet::changePosition(int xx, int yy)
{
  angle = 0;
  speed = 0;
  old_xy.x = xx;
  old_xy.y = yy;
  old_time = std::clock();
  setPosition(xx, yy);
}

void Racquet::updatePosition(int mouse_x)
{
  if (getPosition().x - old_xy.x > 0)
    angle = 0;
  else
    angle = 1 * Utils::Pi;
  
  std::clock_t now = std::clock();

  if (speed and getPosition().x == old_xy.x)
    speed /= 1.4;
  else if (getPosition().x != old_xy.x && now != old_time)
  {
    if (getPosition().x > old_xy.x)
      speed = 2000 * (getPosition().x - old_xy.x) / (now - old_time);
    else
      speed = 2000 * (old_xy.x - getPosition().x) / (now - old_time);
  }
  
  old_xy.x = getPosition().x;
  old_xy.y = getPosition().y;
  
  if (not (mouse_x < Utils::left_wall_d + int(length / 2) || mouse_x > int(Utils::right_wall_d - length / 2)))
    setPosition(mouse_x, Utils::catchline_y);
  else if (mouse_x < Utils::left_wall_d + int(length / 2))
    setPosition(Utils::left_wall_d + int(length / 2), Utils::catchline_y);
  else
    setPosition(Utils::right_wall_d - length / 2, Utils::catchline_y);

  old_time = std::clock();
}

Utils::Borders Racquet::getBorders() const
{
  Utils::Borders borders {int(getPosition().x - length / 2), int(getPosition().y - height / 2), 
                          int(getPosition().x + length / 2), int(getPosition().y + height / 2)};

  return borders;
}

void Racquet::draw(sf::RenderTarget& target, sf::RenderStates states) const
{

  std::vector<sf::Vertex> vertices;
  std::vector<std::vector<int>> sign {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}};

  for (unsigned i = 0; i < sign.size(); i++)
    vertices.push_back(sf::Vertex(sf::Vector2f(getPosition().x + sign[i][0] * length / 2, getPosition().y + sign[i][1] * height / 2)));

  target.draw(&vertices[0], vertices.size(), sf::TrianglesStrip);
}
