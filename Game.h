#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Ball.h"
#include "Racquet.h"
#include "Wall.h"
#include <memory>
#include "Utils.h"
#include <time.h>


class Game
{
  Ball ball;
  Racquet racquet;
  std::vector<std::unique_ptr<Wall>> walls;

  sf::RenderWindow& window;

  std::time_t start_time;
  int score;

  sf::Music wall_sound;
  sf::Music racquet_sound;
  sf::Music racquet2_sound;
  sf::Music applause_sound;
  sf::Music loss_sound;
  sf::Music music;

public:
  Game(sf::RenderWindow& win);

  void run();

private:  
  void music_init(sf::Music& wall_s, sf::Music& racquet_s, sf::Music& racquet2_s,
                sf::Music& applause_s, sf::Music& loss_s, sf::Music& music_s);
  void update(sf::RenderWindow& window);
  void show(sf::RenderWindow& window) const;

  bool areColliding(Thing* thing1, Thing* thing2) const;
  void handleBallCollision(const Utils::Borders& borders, const int thing_speed, const double thing_angle);
  double balanceAngle(double a) const;

  bool isOver() const;
  void waitForSpace() const;

  void putScoreUp();
  void readScoreIn(std::vector<long>& score_table) const;
  unsigned putScoreInPlace(std::vector<long>& score_table) const;
  void printScore(std::vector<long>& score_table, unsigned place) const;
  void saveScore(std::vector<long>& score_table) const;
};

#endif
